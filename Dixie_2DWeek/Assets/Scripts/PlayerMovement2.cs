﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerMovement2 : MonoBehaviour
{
    //Use a vector
    //apply vector to a rigidboy using numbers from input

    Rigidbody2D rB2D;

    public float runSpeed;
    public float jumpForce;

    public SpriteRenderer spriteRenderer;
    public Animator animator;

    public int count;

    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
    }
    private void FixedUpdate()
    {
        //Apply horizontal run movement
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x < 0)
            spriteRenderer.flipX = false; //going left, flips sprite to face direction
        else
        if (rB2D.velocity.x > 0)
            spriteRenderer.flipX = true; //going right, flips sprite to face direction

        //do run animation when moving left and right
        if (Mathf.Abs(horizontalInput) > 0f)
            animator.SetBool("IsRunning", true);
        else
            animator.SetBool("IsRunning", false);
    }
    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
        if (count >= 9)
        {
            winTextObject.SetActive(true);
        }
    }

    //when the player collides with pickup, it adds to score
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }
    }
    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }
}